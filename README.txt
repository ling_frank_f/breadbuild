@author Frank Ling

Bread Builder
=============

This is a program that is used to create new bread formulas

Outline of steps:

1) choose flour blend by percentages
    - use a dict to store flour names as keys, values are percentages

2) choose hydration percentage
    - wet ingredients as dict: keys are names, values are percentages

3) choose dry ingredients
    - ingredients will be in a dictionary with ingredient name as key
    - values will be percentage


